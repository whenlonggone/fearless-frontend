window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = 'http://localhost:8000/api/locations/';
    try {
        const response = await fetch(locationUrl);
        const locationSelector = document.querySelector("#location");
        if (response.ok) {
            const listOfLocations = await response.json();
            console.log(listOfLocations);
            for (let location of listOfLocations.locations) {
                const option = document.createElement('option');
                option.innerHTML = location.name;
                option.value = location.id;
                locationSelector.appendChild(option);
            }
        }

        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json'
                },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
                console.log(newConference);
            }
        })

    } catch {

    }
});
