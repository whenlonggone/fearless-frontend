function createCard(name, description, pictureUrl, date, location) {
    return `
        <div class="card shadow-lg mb-3">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-body-secondary text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">${date}</li>
            </ul>
        </div>
    `;
}

function errorMessage() {
    console.log("error");
    return `
        <div class='alert alert-danger'>Uh oh, something went wrong. Unable to fetch data :(</div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    console.log('ran');
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url)

        if (!response.ok) {
            // Figure out what to do when the response is bad
            const container = document.querySelector('.container');
            container.innerHTML = errorMessage();
        } else {
            const data = await response.json();
            let i = 1;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details);
                    const description = details.conference.description;
                    const title = details.conference.name;
                    const pictureUrl = details.conference.location.picture_url;
                    const dateStart = new Date(details.conference.starts);
                    const dateEnd = new Date(details.conference.ends);
                    const date = dateStart.toLocaleDateString() + " - " + dateEnd.toLocaleDateString();
                    const location = details.conference.location.name;
                    const html = createCard(title,description, pictureUrl, date, location);
                    const column = document.getElementById(`col${i}`);

                    column.innerHTML += html;
                    if (i === 3) {
                        i = 1;
                    } else {
                        i++;
                    }
                } else {
                    const container = document.querySelector('.container');
                    container.innerHTML = errorMessage();
                }
            }

        }
    } catch (e) {
        // Figure out what to do if an error is raised
        const container = document.querySelector('.container');
        container.innerHTML = errorMessage();
    }
});
