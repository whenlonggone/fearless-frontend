window.addEventListener('DOMContentLoaded', async () => {
    const statesUrl = 'http://localhost:8000/api/states/';
    try {
        const response = await fetch(statesUrl);
        const stateSelection = document.querySelector("#state");
        if (response.ok) {
            const listOfStates = await response.json();
            console.log(listOfStates);
            for (let state of listOfStates.states) {
                // console.log(state.name);
                const option = document.createElement('option');
                option.innerHTML = state.name;
                option.value = state.abbreviation;
                stateSelection.appendChild(option);
            }
        }

        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json);
            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);
            }
        })
    } catch {

    }
});
